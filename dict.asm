section .text

global find_word
extern string_equals
find_word:
.loop:
	push rdi
	push rsi
	add rsi, 8
	call string_equals
	pop rsi
	pop rdi
	test rax, rax
	je .exists
	mov rsi, [rsi]
	test rsi, rsi
	jnz .loop
	jmp .not_exists
.exists:
	mov rax, rsi
.not_exists:
	xor rax, rax
	ret
