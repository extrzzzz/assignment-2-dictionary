%include "lib.inc"
%include "words.inc"

section .rodata
not_found_key: db "key not found", 0
too_big: db "string size too big", 0

section .bss
buffer: times 256 db 0

section .text
global _start
extern find_word
_start:
		mov rdi, buffer
		mov rsi, 256
		call read_word
		test rax, rax
		jz .size_error
		mov rdi, rax
		mov rsi, next
		push rdx	
		call find_word
		pop rdx
		test rax, rax
		jz .key_error	
		add rax, 9
		add rax, rdx
		mov rdi, rax
		call print_string
		call print_newline
		xor rdi, rdi
		jmp .end		
.size_error:
	mov rdi, too_big
.error:
	call print_error
	call print_newline
	mov rdi, 1
.key_error:
	mov rdi, not_found_key
	jmp .err
.end:
	call exit

print_error:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret
